#!/bin/bash
# Run the unittests
docker compose run npm run test

# Details:
# docker compose run npm run test
# ^^^^^^^^^^^^^^^^^^
# run a one-off command on a service

# docker compose run npm run test
#                    ^^^
# name of the service defined in docker-compose.yml

# docker compose run npm run test
#                        ^^^^^^^^
# overwrite the default command and run the tests. Since the service
# is called npm and the entrypoint for that service is the executable npm
# the local command `npm run test` behaves the same as the above command
