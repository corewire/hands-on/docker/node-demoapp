#!/bin/bash
# Run the development server for this app
docker compose run npm run dev

# Details:
# docker compose run npm run dev
# ^^^^^^^^^^^^^^^^^^
# run a one-off command on a service

# docker compose run npm run dev
#                    ^^^
# name of the service defined in docker-compose.yml

# docker compose run npm run dev
#                        ^^^^^^^^
# overwrite the default command and run development server. Since the service
# is called npm and the entrypoint for that service is the executable npm
# the local command `npm run dev` behaves the same as the above command
