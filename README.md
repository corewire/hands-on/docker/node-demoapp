# Corewire - Training and Consulting - Demo Application

This is an example project that shows how node projects can be developed using Docker. It is part of a hands-on in the [Docker training](https://labs.corewire.de/de/docker/) by [corewire](https://corewire.de/).

## Usage:

```
# Installation
docker-compose run npm install

# Run
docker-compose run npm run dev

# Test
docker-compose run npm run test
```
