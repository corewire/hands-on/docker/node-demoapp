describe("Basic test example", () => {

  test("should verify the node version", async () => {

    expect(process.version).toBe("v18.16.0");
  });

});